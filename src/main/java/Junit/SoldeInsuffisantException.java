package Junit;

public class SoldeInsuffisantException extends Exception
{
    public SoldeInsuffisantException(String s)
    {
        super(s);
    }
}
