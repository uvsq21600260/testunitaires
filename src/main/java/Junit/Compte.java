package Junit;

public class Compte
{

    private int solde;

    Compte(int solde)
    {
        this.solde=0;
    }

    public int getSolde()
    {
        return solde;
    }

    public void credit(int montant) throws MontantNegatifException
    {
        if(montant<0)
            throw new MontantNegatifException("Le montant est négatif");
        solde+=montant;
    }

    public void debit(int montant) throws SoldeInsuffisantException
    {
        if(montant>solde)
            throw new SoldeInsuffisantException("Compte à Découvert");
        else
            solde-=montant;
    }

    public void virement(Compte n,int montant) throws MontantNegatifException, SoldeInsuffisantException
    {
        if(solde<montant)
            throw new SoldeInsuffisantException("Pas assez d'argent sur le solde");
        solde-=montant;
        n.credit(montant);
    }
}
