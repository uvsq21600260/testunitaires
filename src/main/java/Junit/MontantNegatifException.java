package Junit;

public class MontantNegatifException extends Exception
{
    public MontantNegatifException(String s)
    {
        super(s);
    }
}
