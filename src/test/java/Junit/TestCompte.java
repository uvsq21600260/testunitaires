package Junit;
import org.junit.*;
import static org.junit.Assert.*;

public class TestCompte
{
    @Test
    public void testSoldePositif()
    {
        Compte c=new Compte(100);
        assertTrue(c.getSolde()>0);
    }

    @Test
    public void testCredit()
    {
        Compte c=new Compte(100);
        c.credit(52);
        assertEquals(152,c.getSolde());
    }
}
